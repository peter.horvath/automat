package de.horvath;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
		Machine machine = new Machine();
		machine.setCoins(new double[] {0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1, 2});
		machine.addDrink("Apfelschorle", 5, 1);
		machine.addDrink("Coca Cola", 5, 0.7);
		Drink pepsi = machine.addDrink("Pepsi Cola", 5, 0.6);
		machine.addDrink("Mineralwasser", 10, 0.5);

		System.out.println("init status:");
		machine.showStatus();

		System.out.println("drop in 0.2+0.5+0.5 euro, then buy a pepsi:");
		String result = machine.drinkAndChange(pepsi, 0.2, 0.5, 0.5);

		System.out.println("result: "+result);
		
		System.out.println("end status:");
		machine.showStatus();
    }
}
