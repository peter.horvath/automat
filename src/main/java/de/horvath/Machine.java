package de.horvath;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Machine {
	private double[] coins;
	private int[] coinBox;

	private double reserve = 0;
	private double balance = 0;

	private TreeMap<String, Drink> drinks = new TreeMap<String, Drink>();

	public double[] getCoins() {
		return this.coins;
	}

	public void setCoins(double[] coins) {
		this.coins = coins;
		this.coinBox = new int[coins.length];
		for (int n=0; n<coins.length; n++)
			this.coinBox[n] = 0;
	}
	
	public double getReserve() {
		return this.reserve;
	}

	public void setReserve(double reserve) {
		this.reserve = reserve;
	}

	public double getBalance() {
		return this.balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public Drink addDrink(String name, int reserve, double price) {
		Drink drink = new Drink(name, reserve, price);
		this.drinks.put(name, drink);
		return drink;
	}

	public String drinkAndChange(Drink selection, double... coinInput) {
		for (int n=0; n<coinInput.length; n++) {
			int m;
			for (m=0; m<this.coins.length; m++)
				if (this.coins[m] == coinInput[n]) {
					this.coinBox[m]++;
					break;
				}
			if (m == this.coins.length)
				return "coin " + coinInput[n] + " not acceptable";
			this.balance+=coinInput[n];
			this.reserve+=coinInput[n];
		}

		if (this.balance < selection.getPrice())
			return "price of " + selection.getName() + " is " + selection.getPrice() + ", current balance is only " + this.balance;
		
		if (selection.getReserve()<1)
			return "reserve of " + selection.getName() + " is unfortunately empty";

		selection.setReserve(selection.getReserve()-1);
		this.balance-=selection.getPrice();

		// calculating money back
		LinkedList<String> result=new LinkedList<String>();
		for (int n=this.coins.length-1; n>=0; n--)
			if (this.coinBox[n]>0 && this.balance>this.coins[n]) {
				result.add("a coin of " + this.coins[n] + " gave back");
				this.coinBox[n]--;
				this.balance-=this.coins[n];
				this.reserve-=this.coins[n];
			}
		
		result.add(selection.getName() + " is ready");
		return String.join("\n", result);
	}

	public void Machine() {
	}

	public void showStatus() {
		System.out.println("reserve: "+this.reserve);
		System.out.println("balance: "+this.balance);

		LinkedList<String> coins=new LinkedList<String>();
		for (int n=0; n<this.coins.length; n++)
			coins.push(""+this.coinBox[n]+"x"+this.coins[n]);
		System.out.println("coins: "+String.join(", ", coins));
		
		for (Map.Entry<String, Drink> entry : this.drinks.entrySet()) {
			Drink drink = entry.getValue();
			System.out.println(drink.getName() + " (" + drink.getPrice() + "): " + drink.getReserve());
		}

		System.out.println("");
	}
}
