package de.horvath;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Drink {
	private String name;
	private int reserve;
	private double price;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getReserve() {
		return this.reserve;
	}

	public void setReserve(int reserve) {
		this.reserve = reserve;
	}

	public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Drink() {
		setReserve(0);
	}

	public Drink(String name, int reserve, double price) {
		setName(name);
		setReserve(reserve);
		setPrice(price);
	}
}
