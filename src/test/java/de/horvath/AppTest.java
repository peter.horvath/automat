package de.horvath;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import de.horvath.Drink;
import de.horvath.Machine;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName ) {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() {
		Machine machine = new Machine();
		machine.setCoins(new double[] {0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1, 2});
		Drink pepsi = machine.addDrink("Pepsi Cola", 5, 0.6);
		String result = machine.drinkAndChange(pepsi, 0.2, 0.5, 0.5);
        assertTrue( machine.getBalance() < 0.1 );
        assertTrue( machine.getBalance() > 0.09 );
    }
}
